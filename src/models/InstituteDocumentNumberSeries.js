import {EdmMapping} from "@themost/data";
import DocumentNumberSeries from './DocumentNumberSeries';

@EdmMapping.entityType('InstituteDocumentNumberSeries')
class InstituteDocumentNumberSeries extends DocumentNumberSeries {
    constructor() {
        super();
    }
    /**
     *
     * @param {*} index
     * @param {DocumentNumberFormatOptions=} formatOptions
     * @return {string}
     */
    async format(index, formatOptions) {
        const finalFormatOptions = Object.assign({
            currentDate: new Date()
        }, formatOptions)
        return super.format(index, finalFormatOptions);
    }
}
module.exports = InstituteDocumentNumberSeries;