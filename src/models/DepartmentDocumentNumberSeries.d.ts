import {EdmMapping, EdmType, DataObject} from '@themost/data';
import DocumentNumberSeries from './DocumentNumberSeries';
/**
 * @class
 */
declare class DepartmentDocumentNumberSeries extends DocumentNumberSeries {

}

export default DepartmentDocumentNumberSeries;