import {IApplication} from '@themost/common';
import { DocumentNumberService } from './DocumentNumberService';
import {DataContext} from '@themost/data';
import DocumentNumberSeries from './models/DocumentNumberSeries';
import DocumentNumberSeriesItem from './models/DocumentNumberSeriesItem';
export declare class DefaultDocumentNumberService extends DocumentNumberService {
    constructor(app: IApplication);
    next(context: DataContext, documentSeries: DocumentNumberSeries, extraAttributes?: any): any;
    add(context: DataContext, file: string, item: DocumentNumberSeriesItem): any;
    replace(context: DataContext, file: string, item: DocumentNumberSeriesItem): any;
}